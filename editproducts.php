<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Get Variables From The Form
    $proid          = $_POST['id'];
    $proName        = $_POST['name'];
    $proFact        = $_POST['fact'];

    $prodRow = getOneFrom("`products`.*, `factory`.`factoryName`", "products LEFT JOIN `factory` ON `products`.`productsFactoryId` = `factory`.`idFactory` ", "idProducts =" . $proid);

    $prodRow['productsName'] == $proName ? $check = 0 : $check = checkItem("productsName", "products", $proName);

    if ($check == 1) {
        $theMsg = 'اسم المستخدم موجود بالفعل في قواعد البيانات';
        $stat = false;
    } else {

        $stmt = $con->prepare("UPDATE `products` 
        SET `productsName` = ?, `productsFactoryId` = ?
        WHERE `products`.`idProducts` = '" . $proid . "'");
        $stmt->execute([$proName, $proFact]);

        // Echo Success Message

        $theMsg = ' تم تعديل بيانات المنتج ' . $proName . " برقم " .  $proid;
        $stat = true;
        $prodRow = getOneFrom('*', "products", "idProducts = '" . $proid . "'");
    }
} elseif (isset($_GET['prod'])) {

    if (checkItem("idProducts", "products", $_GET['prod'])) {
        $prodRow = getOneFrom('*', "products", "idProducts = '" . $_GET['prod'] . "'");
    } else {
        header('Location: products.php');
        exit();
    }
} else {
    header('Location: products.php');
    exit();
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>إضافه منتج جديد</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($theMsg) && $stat == true) { ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <?php if (isset($theMsg) && $stat == false) { ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                            <input type="hidden" required="required" name="id" autocomplete="off" value="<?= ($_GET['prod']) ?>">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">اسم المنتج
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="name" autocomplete="off" value="<?= ($prodRow['productsName']) ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">المصنع
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" required name="fact">
                                        <option disabled>اختار المصنع</option>
                                        <?php
                                        $fact = getAllFrom('*', 'factory');
                                        foreach ($fact as $key) {
                                            if ($key['idFactory'] == $prodRow['productsFactoryId']) {
                                                echo "<option selected value='" . $key['idFactory'] . "'>" . $key['factoryName'] . "</option>";
                                            } else {
                                                echo "<option value='" . $key['idFactory'] . "'>" . $key['factoryName'] . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary col-sm-12">عدل</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'layout/footer.php';
ob_end_flush();
?>