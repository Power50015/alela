<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Get Variables From The Form
    $custid         = $_POST['id'];
    $custName         = $_POST['name'];
    $_POST['user'] == "" ? $custUser = Null : $custUser = $_POST['user'];
    $_POST['pass'] == "" ? $custPass = Null : $custPass = $_POST['pass'];

    $costRow = getOneFrom('*', "customer", "idCustomer = '".$custid."'");

    $costRow['customerName'] == $custName?$check = 0 :$check = checkItem("customerName", "customer", $custName);
    $costRow['customerUser'] == $custUser?$check = 0 :$check = checkItem("customerUser", "customer", $custUser);

    if ($check == 1) {
        $theMsg = 'اسم المستخدم موجود بالفعل في قواعد البيانات';
        $stat = false;
    } else {

        $stmt = $con->prepare("UPDATE `customer` 
        SET `customerName` = ?, `customerUser` = ?, `customerPassword` = ?
        WHERE `customer`.`idCustomer` = '".$custid."'");
        $stmt->execute([$custName, $custUser, $custPass]);
        
        // Echo Success Message

        $theMsg = ' تم تعديل بيانات العميل ' . $custName . " برقم " .  $custid;
        $stat = true;
        $costRow = getOneFrom('*', "customer", "idCustomer = '".$custid."'");
    }
} elseif (isset($_GET['cost'])) {

    if(checkItem("idCustomer", "customer", $_GET['cost'])){
        $costRow = getOneFrom('*', "customer", "idCustomer = '".$_GET['cost'] ."'");
    }
    else{
        header('Location: customers.php'); 
        exit();
    }
}else{
    header('Location: customers.php'); 
    exit();
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>تعديل بيانات عميل</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($theMsg) && $stat == true) { ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <?php if (isset($theMsg) && $stat == false) { ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                            <input type="hidden" required="required" name="id" autocomplete="off" value="<?=($_GET['cost'])?>">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">اسم العميل
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="name" autocomplete="off" value="<?=($costRow['customerName'])?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">اسم المستخدم
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="user" class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?=($costRow['customerUser'])?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">كلمه السر</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="middle-name" class="form-control col-md-7 col-xs-12" autocomplete="off" type="text" name="pass" value="<?=($costRow['customerPassword'])?>" >
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary col-sm-12">تعديل</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'layout/footer.php';
ob_end_flush();
?>