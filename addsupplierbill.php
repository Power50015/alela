<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $billId = $_POST['billid'];
    // Date Format issue
    $billDate = $_POST['billdate'];
    $newDate = explode("/", $billDate);
    $billDate = array();
    $billDate[0] = $newDate[2];
    $billDate[1] = $newDate[0];
    $billDate[2] = $newDate[1];
    $newDate = implode("/", $billDate);

    // Date Format issue
    $billDate2 = $_POST['shipmentdate'];
    $newDate2 = explode("/", $billDate2);
    $billDate2 = array();
    $billDate2[0] = $newDate2[2];
    $billDate2[1] = $newDate2[0];
    $billDate2[2] = $newDate2[1];
    $shipmentSupplierDate = implode("/", $billDate2);

    $spId = $_POST['spid'];

    $store = $_POST['store'];

    // Uploade Img

    $target_dir = "upload/sbill/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $stat = true;
    $theMsg = "";
    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            $theMsg = $theMsg . " لم يتم رفع الصورة";
            $uploadOk = 0;
            $stat = false;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $theMsg = $theMsg . " صورة الفاتورة موجودة بالفعل";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $theMsg = $theMsg . " حجم الصور أكبر من 5 ميجا";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    ) {
        $theMsg = $theMsg . " تحقق من صيغة الصورة";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $stat = false;
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            // Check If Category Exist in Database

            $check = checkItem("idSupplierBill", "supplierbill", $billId);

            if ($check == 1) {

                $theMsg = $theMsg . ' رقم الفاتورة موجود بالفعل في قواعد البيانات';
                $stat = false;
            } else {

                $stmt = $con->prepare("INSERT INTO 
                supplierbill(`idSupplierBill`, `supplierbillDate`, `supplierbillImg`, `supplierbillUserId`, `supplierbillSupplierId`)
                VALUES(:zidSupplierBill, :zsupplierbillDate, :zsupplierbillImg, :zsupplierbillUserId, :zsupplierbillSupplierId)");
                $stmt->execute(array(
                    'zidSupplierBill'             => $billId,
                    'zsupplierbillDate'           => $newDate,
                    'zsupplierbillImg'            => $target_file,
                    'zsupplierbillUserId'         => $_SESSION['userID'],
                    'zsupplierbillSupplierId'     => $spId
                ));

                $stmt = $con->prepare("INSERT INTO 
                shipmentsupplier(`idShipmentSupplier`, `shipmentSupplierDate`, `shipmentSupplierStoreId`, `shipmentsupplierSupplierBillId`)
                VALUES          (:zidShipmentSupplier, :zshipmentSupplierDate, :zshipmentSupplierStoreId, :zshipmentsupplierSupplierBillId)");
                $stmt->execute(array(
                    'zidShipmentSupplier'             => $billId,
                    'zshipmentSupplierDate'           => $shipmentSupplierDate,
                    'zshipmentSupplierStoreId'        => $store,
                    'zshipmentsupplierSupplierBillId' => $billId,
                ));

                // Echo Success Message

                $theMsg = " تم إضافه الفاتورة الواردة  برقم " .  $billId;
                $stat = true;
                header('refresh:0;url=addsupplierbillpro.php?bill=' . $billId); // Redirect To login Page
            }
        } else {
            $theMsg = $theMsg . " لم يتم رفع الصورة";
            $stat = false;
        }
    }
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>إضافه فاتورة واردة جديد</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($theMsg) && $stat == true) { ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <?php if (isset($theMsg) && $stat == false) { ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">رقم الفاتورة
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="billid" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">تاريخ الفاتورة
                                    <span class="required">*</span>
                                </label>
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="col-md-8 col-sm-8 col-xs-12 xdisplay_inputx form-group has-feedback">
                                                <input type="text" name="billdate" class="form-control has-feedback-left" id="single_cal1" aria-describedby="inputSuccess2Status">
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">تاريخ استلام الشحنه
                                    <span class="required">*</span>
                                </label>
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="col-md-8 xdisplay_inputx form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="single_cal4" name="shipmentdate" aria-describedby="inputSuccess2Status4">
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">المورد
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" required name="spid">
                                        <option disabled selected>اختار المورد</option>
                                        <?php
                                        $fact = getAllFrom('*', 'supplier');
                                        foreach ($fact as $key) {
                                            echo "<option value='" . $key['idSupplier'] . "'>" . $key['supplierName'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">مخزن تخزين الشحنه
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" required name="store">
                                        <option disabled selected>اختار المخزن</option>
                                        <?php
                                        $fact = getAllFrom('*', 'store');
                                        foreach ($fact as $key) {
                                            echo "<option value='" . $key['idStore'] . "'>" . $key['storeName'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">صورة الفاتورة
                                    <span class="required">*</span>
                                </label>
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="col-md-8 col-sm-8 col-xs-12 xdisplay_inputx form-group has-feedback">
                                                <input type="file" class="form-control has-feedback-left" id="single_cal1" name="fileToUpload">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class=" ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary col-sm-12">اضف</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'layout/footer.php';
exit();
ob_end_flush();
?>