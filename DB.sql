-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2020 at 06:06 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11
SET
  SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET
  AUTOCOMMIT = 0;

START TRANSACTION;

SET
  time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;

/*!40101 SET NAMES utf8mb4 */
;

--
-- Database: `alela`
--
-- --------------------------------------------------------
--
-- Table structure for table `billreleased`
--
CREATE TABLE `billreleased` (
  `idBillReleased` varchar(11) NOT NULL,
  `billReleasedDate` date NOT NULL,
  `billReleasedTotal` float NOT NULL,
  `billReleasedCustomerId` varchar(11) NOT NULL,
  `billReleasedUserId` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `billreleased_products`
--
CREATE TABLE `billreleased_products` (
  `idBillReleasedBillreleased_products` varchar(11) NOT NULL,
  `idProductsBillreleased_products` int(11) NOT NULL,
  `billreleased_productsQuantity` int(11) NOT NULL,
  `billreleased_productsPriceProduct` float NOT NULL,
  `billreleased_productsPriceBuy` float NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `customer`
--
CREATE TABLE `customer` (
  `idCustomer` varchar(11) NOT NULL,
  `customerName` varchar(1500) NOT NULL,
  `customerUser` varchar(255) DEFAULT NULL,
  `customerPassword` varchar(11) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `customer`
--
INSERT INTO
  `customer` (
    `idCustomer`,
    `customerName`,
    `customerUser`,
    `customerPassword`
  )
VALUES
  ('50099706136', 'عميل 2', NULL, NULL),
  ('51545907978', 'عميل 1', NULL, NULL);

-- --------------------------------------------------------
--
-- Table structure for table `driver`
--
CREATE TABLE `driver` (
  `idDriver` varchar(11) NOT NULL,
  `driverName` varchar(1500) NOT NULL,
  `driverPhone` varchar(12) NOT NULL,
  `driverCar` varchar(1500) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `driver`
--
INSERT INTO
  `driver` (
    `idDriver`,
    `driverName`,
    `driverPhone`,
    `driverCar`
  )
VALUES
  ('01994309030', ' سائق 2', '4321', ' سائق 2'),
  (
    '23060792513',
    'أحمد خليل',
    '01116325144',
    'نقل كبيرة fiat'
  ),
  (
    '30541582090',
    'باسم النخيلى',
    '01117474777',
    'نصف نقل'
  ),
  ('96558354888', 'سائق 1', '7185538740', 'عربة 1');

-- --------------------------------------------------------
--
-- Table structure for table `duecustomer`
--
CREATE TABLE `duecustomer` (
  `idDueCustomer` int(11) NOT NULL,
  `dueCustomerDate` date NOT NULL,
  `dueCustomertype` tinyint(1) NOT NULL,
  `dueCustomerIdbill` varchar(11) NOT NULL,
  `dueCustomerIdCustomer` varchar(11) NOT NULL,
  `dueCustomerIdUser` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `duesupplier`
--
CREATE TABLE `duesupplier` (
  `idDueCustomesupplier` int(11) NOT NULL,
  `dueSupplierDate` date NOT NULL,
  `dueSupplierType` tinyint(1) NOT NULL COMMENT '// 0 عليا 1 دفعت',
  `dueSupplierMonay` float NOT NULL,
  `dueSupplierIdbill` varchar(11) NOT NULL,
  `dueSupplierIdSupplier` varchar(11) NOT NULL,
  `dueSupplierIdUser` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `factory`
--
CREATE TABLE `factory` (
  `idFactory` int(11) NOT NULL,
  `factoryName` varchar(250) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `moved`
--
CREATE TABLE `moved` (
  `idMoved` int(11) NOT NULL,
  `movedDate` date NOT NULL,
  `movedDriverId` varchar(11) NOT NULL,
  `movedReleasedStoreId` int(11) NOT NULL,
  `movedSuppledStoreId` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `moved_products`
--
CREATE TABLE `moved_products` (
  `moved_productsIdProducts` int(11) NOT NULL,
  `moved_productsIdMove` int(11) NOT NULL,
  `moved_productsQuantity` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `products`
--
CREATE TABLE `products` (
  `idProducts` int(11) NOT NULL,
  `productsName` varchar(1500) NOT NULL,
  `productsFactoryId` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `shipmentreleased`
--
CREATE TABLE `shipmentreleased` (
  `idShipmentreleased` int(11) NOT NULL,
  `shipmentreleasedDate` date NOT NULL,
  `shipmentreleasedDriverId` varchar(11) NOT NULL,
  `shipmentreleasedStoreId` int(11) NOT NULL,
  `shipmentreleasedUserId` varchar(11) NOT NULL,
  `shipmentreleasedBillReleasedId` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `shipmentsupplier`
--
CREATE TABLE `shipmentsupplier` (
  `idShipmentSupplier` int(11) NOT NULL,
  `shipmentSupplierDate` date NOT NULL,
  `shipmentSupplierStoreId` int(11) NOT NULL,
  `shipmentsupplierSupplierBillId` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `shipmentsupplier`
--
INSERT INTO
  `shipmentsupplier` (
    `idShipmentSupplier`,
    `shipmentSupplierDate`,
    `shipmentSupplierStoreId`,
    `shipmentsupplierSupplierBillId`
  )
VALUES
  (123, '2020-02-14', 2, '123');

-- --------------------------------------------------------
--
-- Table structure for table `store`
--
CREATE TABLE `store` (
  `idStore` int(11) NOT NULL,
  `storeName` varchar(150) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;



-- --------------------------------------------------------
--
-- Table structure for table `supplier`
--
CREATE TABLE `supplier` (
  `idSupplier` varchar(11) NOT NULL,
  `supplierName` varchar(1500) NOT NULL,
  `supplierUser` varchar(250) DEFAULT NULL,
  `supplierPassword` varchar(250) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;


-- --------------------------------------------------------
--
-- Table structure for table `supplierbill`
--
CREATE TABLE `supplierbill` (
  `idSupplierBill` varchar(11) NOT NULL,
  `supplierbillDate` date NOT NULL,
  `supplierbillTotal` float DEFAULT NULL,
  `supplierbilldiscount` float DEFAULT NULL,
  `supplierbillImg` varchar(500) DEFAULT NULL,
  `supplierbillUserId` varchar(11) DEFAULT NULL,
  `supplierbillSupplierId` varchar(11) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;


-- --------------------------------------------------------
--
-- Table structure for table `supplierbill_products`
--
CREATE TABLE `supplierbill_products` (
  `idSupplierBillSupplierbill_products` varchar(11) NOT NULL,
  `idProductsSupplierbill_products` int(11) NOT NULL,
  `supplierbill_productsQuantity` int(11) NOT NULL,
  `supplierbill_productsPriceProduct` float NOT NULL,
  `supplierbill_productsPriceBuy` float NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `user`
--
CREATE TABLE `user` (
  `idUser` varchar(11) NOT NULL,
  `userName` varchar(1500) NOT NULL,
  `userUser` varchar(250) NOT NULL,
  `userPassword` varchar(50) NOT NULL,
  `userAccess` varchar(10) NOT NULL,
  `imgUser` varchar(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;



--
-- Indexes for dumped tables
--
--
-- Indexes for table `billreleased`
--
ALTER TABLE
  `billreleased`
ADD
  PRIMARY KEY (`idBillReleased`),
ADD
  KEY `billReleasedCustomerId` (`billReleasedCustomerId`),
ADD
  KEY `billReleasedUserId` (`billReleasedUserId`);

--
-- Indexes for table `billreleased_products`
--
ALTER TABLE
  `billreleased_products`
ADD
  PRIMARY KEY (
    `idBillReleasedBillreleased_products`,
    `idProductsBillreleased_products`
  ),
ADD
  KEY `idProductsBillreleased_products` (`idProductsBillreleased_products`);

--
-- Indexes for table `customer`
--
ALTER TABLE
  `customer`
ADD
  PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `driver`
--
ALTER TABLE
  `driver`
ADD
  PRIMARY KEY (`idDriver`);

--
-- Indexes for table `duecustomer`
--
ALTER TABLE
  `duecustomer`
ADD
  PRIMARY KEY (`idDueCustomer`),
ADD
  KEY `dueCustomerIdbill` (`dueCustomerIdbill`),
ADD
  KEY `dueCustomerIdUser` (`dueCustomerIdUser`),
ADD
  KEY `dueCustomerIdCustomer` (`dueCustomerIdCustomer`);

--
-- Indexes for table `duesupplier`
--
ALTER TABLE
  `duesupplier`
ADD
  PRIMARY KEY (`idDueCustomesupplier`),
ADD
  KEY `dueSupplierIdbill` (`dueSupplierIdbill`),
ADD
  KEY `dueSupplierIdSupplier` (`dueSupplierIdSupplier`),
ADD
  KEY `dueSupplierIdUser` (`dueSupplierIdUser`);

--
-- Indexes for table `factory`
--
ALTER TABLE
  `factory`
ADD
  PRIMARY KEY (`idFactory`);

--
-- Indexes for table `moved`
--
ALTER TABLE
  `moved`
ADD
  PRIMARY KEY (`idMoved`),
ADD
  KEY `movedDriverId` (`movedDriverId`),
ADD
  KEY `movedReleasedStoreId` (`movedReleasedStoreId`),
ADD
  KEY `movedSuppledStoreId` (`movedSuppledStoreId`);

--
-- Indexes for table `moved_products`
--
ALTER TABLE
  `moved_products`
ADD
  PRIMARY KEY (
    `moved_productsIdProducts`,
    `moved_productsIdMove`
  ),
ADD
  KEY `moved_productsIdMove` (`moved_productsIdMove`);

--
-- Indexes for table `products`
--
ALTER TABLE
  `products`
ADD
  PRIMARY KEY (`idProducts`),
ADD
  KEY `productsFactoryId` (`productsFactoryId`);

--
-- Indexes for table `shipmentreleased`
--
ALTER TABLE
  `shipmentreleased`
ADD
  PRIMARY KEY (`idShipmentreleased`),
ADD
  KEY `shipmentreleasedDriverId` (`shipmentreleasedDriverId`),
ADD
  KEY `shipmentreleasedStoreId` (`shipmentreleasedStoreId`),
ADD
  KEY `shipmentreleasedUserId` (`shipmentreleasedUserId`),
ADD
  KEY `shipmentreleasedBillReleasedId` (`shipmentreleasedBillReleasedId`);

--
-- Indexes for table `shipmentsupplier`
--
ALTER TABLE
  `shipmentsupplier`
ADD
  PRIMARY KEY (`idShipmentSupplier`),
ADD
  KEY `shipmentSupplierStoreId` (`shipmentSupplierStoreId`),
ADD
  KEY `shipmentsupplier_ibfk_2` (`shipmentsupplierSupplierBillId`);

--
-- Indexes for table `store`
--
ALTER TABLE
  `store`
ADD
  PRIMARY KEY (`idStore`);

--
-- Indexes for table `supplier`
--
ALTER TABLE
  `supplier`
ADD
  PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `supplierbill`
--
ALTER TABLE
  `supplierbill`
ADD
  PRIMARY KEY (`idSupplierBill`),
ADD
  KEY `supplierbillUserId` (`supplierbillUserId`),
ADD
  KEY `supplierbillSupplierId` (`supplierbillSupplierId`);

--
-- Indexes for table `supplierbill_products`
--
ALTER TABLE
  `supplierbill_products`
ADD
  PRIMARY KEY (
    `idSupplierBillSupplierbill_products`,
    `idProductsSupplierbill_products`
  ),
ADD
  KEY `idProductsSupplierbill_products` (`idProductsSupplierbill_products`);

--
-- Indexes for table `user`
--
ALTER TABLE
  `user`
ADD
  PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--
--
-- AUTO_INCREMENT for table `duecustomer`
--
ALTER TABLE
  `duecustomer`
MODIFY
  `idDueCustomer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `duesupplier`
--
ALTER TABLE
  `duesupplier`
MODIFY
  `idDueCustomesupplier` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 21;

--
-- AUTO_INCREMENT for table `factory`
--
ALTER TABLE
  `factory`
MODIFY
  `idFactory` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 5;

--
-- AUTO_INCREMENT for table `moved`
--
ALTER TABLE
  `moved`
MODIFY
  `idMoved` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE
  `products`
MODIFY
  `idProducts` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 6;

--
-- AUTO_INCREMENT for table `shipmentreleased`
--
ALTER TABLE
  `shipmentreleased`
MODIFY
  `idShipmentreleased` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipmentsupplier`
--
ALTER TABLE
  `shipmentsupplier`
MODIFY
  `idShipmentSupplier` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 12344;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE
  `store`
MODIFY
  `idStore` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 4;

--
-- Constraints for dumped tables
--
--
-- Constraints for table `billreleased`
--
ALTER TABLE
  `billreleased`
ADD
  CONSTRAINT `billreleased_ibfk_1` FOREIGN KEY (`billReleasedCustomerId`) REFERENCES `customer` (`idCustomer`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `billreleased_ibfk_2` FOREIGN KEY (`billReleasedUserId`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `billreleased_products`
--
ALTER TABLE
  `billreleased_products`
ADD
  CONSTRAINT `billreleased_products_ibfk_1` FOREIGN KEY (`idBillReleasedBillreleased_products`) REFERENCES `billreleased` (`idBillReleased`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `billreleased_products_ibfk_2` FOREIGN KEY (`idProductsBillreleased_products`) REFERENCES `products` (`idProducts`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `duecustomer`
--
ALTER TABLE
  `duecustomer`
ADD
  CONSTRAINT `duecustomer_ibfk_1` FOREIGN KEY (`dueCustomerIdbill`) REFERENCES `billreleased` (`idBillReleased`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `duecustomer_ibfk_2` FOREIGN KEY (`dueCustomerIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `duecustomer_ibfk_3` FOREIGN KEY (`dueCustomerIdCustomer`) REFERENCES `customer` (`idCustomer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `duesupplier`
--
ALTER TABLE
  `duesupplier`
ADD
  CONSTRAINT `duesupplier_ibfk_1` FOREIGN KEY (`dueSupplierIdbill`) REFERENCES `supplierbill` (`idSupplierBill`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `duesupplier_ibfk_2` FOREIGN KEY (`dueSupplierIdSupplier`) REFERENCES `supplier` (`idSupplier`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `duesupplier_ibfk_3` FOREIGN KEY (`dueSupplierIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `moved`
--
ALTER TABLE
  `moved`
ADD
  CONSTRAINT `moved_ibfk_1` FOREIGN KEY (`movedDriverId`) REFERENCES `driver` (`idDriver`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `moved_ibfk_2` FOREIGN KEY (`movedReleasedStoreId`) REFERENCES `store` (`idStore`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `moved_ibfk_3` FOREIGN KEY (`movedSuppledStoreId`) REFERENCES `store` (`idStore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `moved_products`
--
ALTER TABLE
  `moved_products`
ADD
  CONSTRAINT `moved_products_ibfk_1` FOREIGN KEY (`moved_productsIdProducts`) REFERENCES `products` (`idProducts`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `moved_products_ibfk_2` FOREIGN KEY (`moved_productsIdMove`) REFERENCES `moved` (`idMoved`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE
  `products`
ADD
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`productsFactoryId`) REFERENCES `factory` (`idFactory`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shipmentreleased`
--
ALTER TABLE
  `shipmentreleased`
ADD
  CONSTRAINT `shipmentreleased_ibfk_1` FOREIGN KEY (`shipmentreleasedDriverId`) REFERENCES `driver` (`idDriver`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `shipmentreleased_ibfk_2` FOREIGN KEY (`shipmentreleasedStoreId`) REFERENCES `store` (`idStore`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `shipmentreleased_ibfk_3` FOREIGN KEY (`shipmentreleasedUserId`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `shipmentreleased_ibfk_4` FOREIGN KEY (`shipmentreleasedBillReleasedId`) REFERENCES `billreleased` (`idBillReleased`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shipmentsupplier`
--
ALTER TABLE
  `shipmentsupplier`
ADD
  CONSTRAINT `shipmentsupplier_ibfk_1` FOREIGN KEY (`shipmentSupplierStoreId`) REFERENCES `store` (`idStore`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `shipmentsupplier_ibfk_2` FOREIGN KEY (`shipmentsupplierSupplierBillId`) REFERENCES `supplierbill` (`idSupplierBill`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplierbill`
--
ALTER TABLE
  `supplierbill`
ADD
  CONSTRAINT `supplierbill_ibfk_1` FOREIGN KEY (`supplierbillUserId`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `supplierbill_ibfk_2` FOREIGN KEY (`supplierbillSupplierId`) REFERENCES `supplier` (`idSupplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplierbill_products`
--
ALTER TABLE
  `supplierbill_products`
ADD
  CONSTRAINT `supplierbill_products_ibfk_1` FOREIGN KEY (`idSupplierBillSupplierbill_products`) REFERENCES `supplierbill` (`idSupplierBill`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `supplierbill_products_ibfk_2` FOREIGN KEY (`idProductsSupplierbill_products`) REFERENCES `products` (`idProducts`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;