<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
if (isset($_GET['bill'])) {
    if (empty($_GET['bill'])) {
        header('Location: login.php'); // Redirect To login Page
        exit();
    }
} else {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

$billDet =     getOneFrom('*', "supplierbill", "idSupplierBill = '" . $_GET['bill'] . "'");
$supplierDet = getOneFrom('*', "supplier", "idSupplier = '" . $billDet["supplierbillSupplierId"] . "'");
$shipmentsupplier = getOneFrom('*', "shipmentsupplier", "shipmentsupplierSupplierBillId = '" . $billDet["idSupplierBill"] . "'");
$store = getOneFrom('*', "store", "idStore = '" . $shipmentsupplier["shipmentSupplierStoreId"] . "'");
$user = getOneFrom('*', "user", "idUser = '" . $billDet["supplierbillUserId"] . "'");
$totalpay = getOneFrom('SUM(`dueSupplierMonay`)', "duesupplier", "`dueSupplierType` = 1 AND `dueSupplierIdbill`  = '" . $billDet["idSupplierBill"] . "'");
$totalwant = getOneFrom('SUM(`dueSupplierMonay`)', "duesupplier", "`dueSupplierType` = 0 AND `dueSupplierIdbill`  = '" . $billDet["idSupplierBill"] . "'");
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>الفـــاتـــورة</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div>
                            <h5 class="col-xs-3 col-sm-3">رقم الفاتورة :<?= ($billDet["idSupplierBill"]) ?></h5>
                            <h5 class="col-xs-3 col-sm-3">تاريخ الفاتورة :<?= ($billDet["supplierbillDate"]) ?></h5>
                            <h5 class="col-xs-3 col-sm-3">صورة الفاتورة :<a class="text-primary" target="_blank" href="<?= ($billDet["supplierbillImg"]) ?>">الفاتورة الأصل</a></h5>
                            <h5 class="col-xs-3 col-sm-3">المورد :<a href="#"><?= ($supplierDet["supplierName"]) ?></a></h5>
                            <br />
                            <h5 class="col-xs-3 col-sm-3">المستخدم :<a class="text-primary" target="_blank" href="#"><?= ($user["userName"]) ?></a></h5>
                            <h5 class="col-xs-3 col-sm-3">تاريخ استلام الفاتورة :<?= ($shipmentsupplier["shipmentSupplierDate"]) ?></h5>
                            <h5 class="col-xs-3 col-sm-3">المخزن :<a class="text-primary" target="_blank" href="#"><?= ($store["storeName"]) ?></a></h5>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <br />
                        <hr />
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>أسم الصنف</th>
                                    <th>سعر الصنف</th>
                                    <th>سعر بيع الصنف</th>
                                    <th>الكمية</th>
                                    <th>الإجمالى</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $allItems = getAllFrom(
                                    " `supplierbill_products`.*, `products`.*",
                                    "`supplierbill_products`",
                                    "LEFT JOIN `products` ON `supplierbill_products`.`idProductsSupplierbill_products` = `products`.`idProducts` WHERE `idSupplierBillSupplierbill_products` = '" . $billDet["idSupplierBill"] . "'"
                                );
                                foreach ($allItems as $value) {
                                ?>
                                    <tr>
                                        <td><?= ($value["productsName"]) ?></td>
                                        <td><?= ($value["supplierbill_productsPriceProduct"]) ?></td>
                                        <td><?= ($value["supplierbill_productsPriceBuy"]) ?></td>
                                        <td><?= ($value["supplierbill_productsQuantity"]) ?></td>
                                        <td><?= ($value["supplierbill_productsPriceBuy"] * $value["supplierbill_productsQuantity"]) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br />
                        <hr />
                        <h5 class="col-xs-3 col-sm-3">إجمالى :<?= ($billDet["supplierbillTotal"]) ?></h5>
                        <h5 class="col-xs-3 col-sm-3">الخصم :<?= ($billDet["supplierbilldiscount"]) ?></h5>
                        <h5 class="col-xs-3 col-sm-3">إجمالى بعد الخصم :<?= ($billDet["supplierbillTotal"] - $billDet["supplierbilldiscount"]) ?></h5>
                        <h5 class="col-xs-3 col-sm-3">إجمالى المدفوع :<?= ($totalpay["SUM(`dueSupplierMonay`)"]) ?></h5>
                        <h5 class="col-xs-3 col-sm-3">إجمالى المتبقى على الفاتورة :<?= ($totalwant["SUM(`dueSupplierMonay`)"] - $totalpay["SUM(`dueSupplierMonay`)"]) ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'layout/footer.php';
ob_end_flush();
?>