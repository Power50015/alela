<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Get Variables From The Form
    $supplierid         = $_POST['id'];
    $supplierName         = $_POST['name'];
    $_POST['user'] == "" ? $supplierUser = Null : $supplierUser = $_POST['user'];
    $_POST['pass'] == "" ? $supplierPass = Null : $supplierPass = $_POST['pass'];

    $supplierRow = getOneFrom('*', "supplier", "idSupplier = '".$supplierid."'");

    $supplierRow['supplierName'] == $supplierName?$check = 0 :$check = checkItem("supplierName", "supplier", $supplierName);
    $supplierRow['supplierUser'] == $supplierUser?$check = 0 :$check = checkItem("supplierUser", "supplier", $supplierUser);

    if ($check == 1) {
        $theMsg = 'اسم المستخدم موجود بالفعل في قواعد البيانات';
        $stat = false;
    } else {

        $stmt = $con->prepare("UPDATE `supplier` 
        SET `supplierName` = ?, `supplierUser` = ?, `supplierPassword` = ?
        WHERE `supplier`.`idSupplier` = '".$supplierid."'");
        $stmt->execute([$supplierName, $supplierUser, $supplierPass]);
        
        // Echo Success Message

        $theMsg = ' تم تعديل بيانات المورد ' . $supplierName . " برقم " .  $supplierid;
        $stat = true;
        $supplierRow = getOneFrom('*', "supplier", "idSupplier = '".$supplierid."'");
    }
} elseif (isset($_GET['supplier'])) {

    if(checkItem("idSupplier", "supplier", $_GET['supplier'])){
        $supplierRow = getOneFrom('*', "supplier", "idSupplier = '".$_GET['supplier'] ."'");
    }
    else{
        header('Location: suppliers.php'); 
        exit();
    }
}else{
    header('Location: suppliers.php'); 
    exit();
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>تعديل بيانات مورد</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($theMsg) && $stat == true) { ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <?php if (isset($theMsg) && $stat == false) { ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?= ($theMsg) ?></strong>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                            <input type="hidden" required="required" name="id" autocomplete="off" value="<?=($_GET['supplier'])?>">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">اسم المورد
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="name" autocomplete="off" value="<?=($supplierRow['supplierName'])?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">اسم المستخدم
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="user" class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?=($supplierRow['supplierUser'])?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">كلمه السر</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="middle-name" class="form-control col-md-7 col-xs-12" autocomplete="off" type="text" name="pass" value="<?=($supplierRow['supplierPassword'])?>" >
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary col-sm-12">تعديل</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'layout/footer.php';
ob_end_flush();
?>