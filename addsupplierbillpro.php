<?php
ob_start();
session_start();
if (!(isset($_SESSION['userID']))) {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
if (isset($_GET['bill'])) {
    if (empty($_GET['bill'])) {
        header('Location: login.php'); // Redirect To login Page
        exit();
    }
} else {
    header('Location: login.php'); // Redirect To login Page
    exit();
}
include_once 'init.php';
include_once 'layout/head.php';
include_once 'layout/header.php';

if (isset($_GET['do'])) {
    if ($_GET['do'] = "del") {
        if (isset($_GET['pro'])) {
            if (!empty($_GET['pro'])) {
                $stmt = $con->prepare("DELETE FROM supplierbill_products WHERE idSupplierBillSupplierbill_products = '" . $_GET['bill'] . "' AND idProductsSupplierbill_products  = '" . $_GET['pro'] . "'");

                $stmt->execute();

                $Msg = "تم حذف الصنف من الفاتورة";
                header('Location: addsupplierbillpro.php?bill=' . $_GET['bill']); // Redirect To login Page
                exit();
            }
        }
    }
}

$billDet =     getOneFrom('*', "supplierbill", "idSupplierBill = '" . $_GET['bill'] . "'");
$supplierDet = getOneFrom('*', "supplier", "idSupplier = '" . $billDet["supplierbillSupplierId"] . "'");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['bi'])) {
        $prodid = $_POST['prodid'];
        $quantity = $_POST['quantity'];
        $price = $_POST['price'];
        $buyprice = $_POST['buyprice'];
        // Insert Category Info In Database

        $stmt = $con->prepare("INSERT INTO supplierbill_products(
        `idSupplierBillSupplierbill_products`, `idProductsSupplierbill_products`, `supplierbill_productsQuantity`, `supplierbill_productsPriceProduct`, `supplierbill_productsPriceBuy`
    ) VALUES (
        :zidSupplierBillSupplierbill_products,:zidProductsSupplierbill_products,:zsupplierbill_productsQuantity,:zsupplierbill_productsPriceProduct,:zsupplierbill_productsPriceBuy)");

        $stmt->execute(array(
            'zidSupplierBillSupplierbill_products'     => $_GET['bill'],
            'zidProductsSupplierbill_products'         => $prodid,
            'zsupplierbill_productsQuantity'           => $quantity,
            'zsupplierbill_productsPriceProduct'       => $price,
            'zsupplierbill_productsPriceBuy'           => $buyprice,

        ));

        header('Location: addsupplierbillpro.php?bill=' . $_GET['bill']); // Redirect To login Page
        exit();
    } elseif (isset($_POST['total'])) {
        $total = $_POST['tot'];
        $dis = $_POST['dis'];
        $need = floatval($total - $dis);
        $pay = $_POST['pay'];
        $duedate = $_POST['duedate'];
        $newDate = explode("/", $duedate);
        $duedate = array();
        $duedate[0] = $newDate[2];
        $duedate[1] = $newDate[0];
        $duedate[2] = $newDate[1];
        $newDate = implode("/", $duedate);
        $dueSupplierIdSupplier =getOneFrom('supplierbillSupplierId', "supplierbill", "idSupplierBill = '" . $_GET['bill'] . "'");

        // total need
        $stmt = $con->prepare("INSERT INTO duesupplier(
            `dueSupplierDate`, `dueSupplierType`, `dueSupplierMonay`,`dueSupplierIdbill`, `dueSupplierIdSupplier`, `dueSupplierIdUser`
        ) VALUES (
            :zdueSupplierDate, :zdueSupplierType, :zdueSupplierMonay, :zdueSupplierIdbill, :zdueSupplierIdSupplier,:zdueSupplierIdUser)");

        $stmt->execute(array(
            'zdueSupplierDate'             => $newDate,
            'zdueSupplierType'             => 0,
            'zdueSupplierMonay'            => $need,
            'zdueSupplierIdbill'           => $_GET['bill'],
            'zdueSupplierIdSupplier'       =>$dueSupplierIdSupplier['supplierbillSupplierId'],
            'zdueSupplierIdUser'           => $_SESSION['userID']
        ));

        // total pay
        $stmt = $con->prepare("INSERT INTO duesupplier(
            `dueSupplierDate`, `dueSupplierType`, `dueSupplierMonay`,`dueSupplierIdbill`, `dueSupplierIdSupplier`, `dueSupplierIdUser`
        ) VALUES (
            :zdueSupplierDate, :zdueSupplierType, :zdueSupplierMonay, :zdueSupplierIdbill, :zdueSupplierIdSupplier,:zdueSupplierIdUser)");

        $stmt->execute(array(
            'zdueSupplierDate'             => $newDate,
            'zdueSupplierType'             => 1,
            'zdueSupplierMonay'            => $pay,
            'zdueSupplierIdbill'           => $_GET['bill'],
            'zdueSupplierIdSupplier'       =>$dueSupplierIdSupplier['supplierbillSupplierId'],
            'zdueSupplierIdUser'           => $_SESSION['userID']
        ));

        $stmt = $con->prepare("UPDATE `supplierbill` 
        SET `supplierbillTotal` = ?, `supplierbilldiscount` = ?
        WHERE `idSupplierBill` = '" . $_GET['bill'] . "'");
        $stmt->execute([$total, $dis]);

        header('Location: showsupplierbillpro.php?bill=' . $_GET['bill']); // Redirect To login Page
        exit();
    }
}
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>إضافة منتجات للفاتورة</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div>
                            <h5 class="col-xs-3 col-sm-3">رقم الفاتورة :<?= ($billDet["idSupplierBill"]) ?></h5>
                            <h5 class="col-xs-3 col-sm-3">تاريخ الفاتورة :<?= ($billDet["supplierbillDate"]) ?></h5>
                            <h5 class="col-xs-3 col-sm-3">صورة الفاتورة :<a class="text-primary" target="_blank" href="<?= ($billDet["supplierbillImg"]) ?>">الفاتورة الأصل</a></h5>
                            <h5 class="col-xs-3 col-sm-3">المورد :<a href="#"><?= ($supplierDet["supplierName"]) ?></a></h5>
                            <br />
                            <br />
                            <br />
                            <br />
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="#" class="btn btn-primary col-sm-12">تعديل البيانات الأولية للفاتورة</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <br />
                        <hr />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                            <h3>إضافة منتجات للفاتورة</h3>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">الصنف
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                    //Where Stmt
                                    $whereStm = getAllFrom('idProductsSupplierbill_products', 'supplierbill_products', "WHERE idSupplierBillSupplierbill_products ='" . $_GET['bill'] . "'");

                                    if (!($whereStm  == NULL)) {
                                        $wherstatment = "WHERE ";
                                        foreach ($whereStm as $key) {
                                            $wherstatment =  $wherstatment . " idProducts != " . $key['idProductsSupplierbill_products'] . ' AND ';
                                        }
                                        $wherstatment = substr($wherstatment, 0, -4);
                                    } else {
                                        $wherstatment = "";
                                    }
                                    ?>
                                    <select class="form-control" required name="prodid">
                                        <option disabled selected>اختار الصنف</option>
                                        <?php
                                        $products = getAllFrom(
                                            '`products`.*, `factory`.`factoryName`',
                                            'products LEFT JOIN `factory` ON `products`.`productsFactoryId` = `factory`.`idFactory`',
                                            $wherstatment
                                        );
                                        foreach ($products as $key) {
                                            echo "<option value='" . $key['idProducts'] . "'> أسم المنتج : " . $key['productsName'] . " || المصنع المنتج : " . $key['factoryName'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">الكمية
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input required type="number" id="quantity" name="quantity" class="form-control col-md-7 col-xs-12" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price"> السعر
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input required type="number" id="price" name="price" class="form-control col-md-7 col-xs-12" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="buyprice">سعر البيع
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input required type="number" id="buyprice" name="buyprice" class="form-control col-md-7 col-xs-12" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" name="bi" class="btn btn-primary col-sm-12">أضف الصنف للفاتورة</button>
                                </div>
                            </div>

                        </form>
                        <hr />
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered dataTable no-footer">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 221px;">الصنف</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 233px;">السعر</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 212px;">سعر البيع</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 141px;">الكمية</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 222px;">السعر الإجمالى</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 212px;">حذف</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $billPro = getAllFrom('*', 'supplierbill_products', "WHERE idSupplierBillSupplierbill_products ='" . $_GET['bill'] . "'");
                                        $billTotla = 0;
                                        foreach ($billPro as $key) {
                                            echo "<tr>";
                                            $productsName = getOneFrom(
                                                'productsName , `factory`.`factoryName`',
                                                "products LEFT JOIN `factory` ON `products`.`productsFactoryId` = `factory`.`idFactory`",
                                                "idProducts = '" .  $key['idProductsSupplierbill_products'] . "'"
                                            );
                                            echo "<td>"  . $productsName['productsName'] . " || المصنع المنتج : " . $productsName['factoryName'] . "</td>";
                                            echo "<td>" . $key["supplierbill_productsPriceProduct"] . "</td>";
                                            echo "<td>" . $key["supplierbill_productsPriceBuy"] . "</td>";
                                            echo "<td>" . $key["supplierbill_productsQuantity"] . "</td>";
                                            echo "<td>" . $key["supplierbill_productsPriceBuy"] * $key["supplierbill_productsQuantity"] . "</td>";
                                            $billTotla = $billTotla + $key["supplierbill_productsPriceBuy"] * $key["supplierbill_productsQuantity"];
                                        ?>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-example-modal-lg-<?= ($key["idProductsSupplierbill_products"]) ?>">حـــــــذف
                                                </button>

                                                <div class="modal fade bs-example-modal-lg-<?= ($key["idProductsSupplierbill_products"]) ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel">حذف الصنف من الفاتورة</h4>
                                                                <h4><?= ($productsName['productsName'] . " || المصنع المنتج : " . $productsName['factoryName']) ?></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>ارجو التأكد قبل الحذف الصنف من الفاتورة</p>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <a href="<?= ("addsupplierbillpro.php?bill=" . $_GET['bill'] . "&do=del&pro=" . $key["idProductsSupplierbill_products"]) ?>" type="button" class="btn btn-danger">حذف على أى حال</a>
                                                                <button name="bi" class="btn btn-primary" data-dismiss="modal">إلغاء عمليه الحذف و الإحتفاظ بالبيانات</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        <?php
                                            echo "</tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <br>
                            <hr />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                                <h3 id="demo">الإجمالى : <?= $billTotla ?></h3>
                                <input type="hidden" value="<?= $billTotla ?>" name="tot" id="total">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dis">الخصم
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="0" required type="number" id="dis" name="dis" class="form-control col-md-7 col-xs-12" autocomplete="off" min="0" max="<?= $billTotla ?>">
                                    </div>
                                </div>
                                <h3 id="demo">الإجمالى بعد الخصم : <span></span></h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pay">المدفوع
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="0" required type="number" id="pay" name="pay" class="form-control col-md-7 col-xs-12" autocomplete="off" min="0" max="<?= $billTotla ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">تاريخ الدفع
                                        <span class="required">*</span>
                                    </label>
                                    <fieldset>
                                        <div class="control-group">
                                            <div class="controls">
                                                <div class="col-md-8 col-sm-8 col-xs-12 xdisplay_inputx form-group has-feedback">
                                                    <input type="text" name="duedate" class="form-control has-feedback-left" id="single_cal1" aria-describedby="inputSuccess2Status">
                                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <h3 id="demo2">المتبقي : <span></span></h3>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button name="total" type="submit" class="btn btn-primary col-sm-12">حفظ الفاتورة</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById("dis").addEventListener("change", displayDate);
    document.getElementById("pay").addEventListener("change", displayDate);

    function displayDate() {

        var total = document.getElementById("total").value - document.getElementById("dis").value;
        document.querySelector("#demo span").innerHTML = total;

        var pay = total - document.getElementById("pay").value;
        document.querySelector("#demo2 span").innerHTML = pay;
    }
</script>
<?php
include_once 'layout/footer.php';
ob_end_flush();
?>