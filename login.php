<?php
	ob_start();
	session_start();
	if (isset($_SESSION['userID'])) {
		header('Location: index.php');
	}
	include 'init.php';

	// Check If User Coming From HTTP Post Request

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if (isset($_POST['login'])) {

			$user = $_POST['username'];
			$pass = $_POST['password'];
			$hashedPass = sha1($pass);

			// Check If The User Exist In Database

			$stmt = $con->prepare("SELECT 
										`idUser`, `userName`, `userAccess` 
									FROM 
										user
									WHERE 
                                        `userUser` = ? 
									AND 
                                        `userPassword` = ?");

			$stmt->execute(array($user, $hashedPass));

			$get = $stmt->fetch();

			$count = $stmt->rowCount();

			// If Count > 0 This Mean The Database Contain Record About This Username

			if ($count > 0) {

        $_SESSION['userID'] = $get['idUser']; // Register User ID in Session

				$_SESSION['userName'] = $get['userName']; // Register Session Name
                
        $_SESSION['userAccess'] = $get['userAccess']; // Register User ID in Session

				header('Location: index.php'); // Redirect To index Page

				exit();
      }

		} 

	}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alela! | لإداره المخازن</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/bootstrap-rtl/dist/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form  action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" >
              <h1>تسجيل الدخول</h1>
              <div>
                <input type="text" class="form-control" placeholder="المستخدم" required="" name="username"  />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="كلمه السر" required="" name="password" />
              </div>
              <div>
                <input class="btn btn-default submit " type="submit" value="تسجيل الدخول" name="login" >
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Alela!</h1>
                  <p>جميع حقوق البرنامج محفوظه ل<a href="#">Power-mostafa.com</a></p>
                  <br><br>
                  <p>© 1397 جميع الحقوق محفوظة. Gentelella Alela! قالب Bootstrap 3. الخصوصية والشروط</p>
                </div>
              </div>
            </form>
          </section>
        </div>
        
      </div>
    </div>
  </body>
</html>
<?php 
	ob_end_flush();
?>