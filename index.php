<?php
	ob_start();
    session_start();
        if (!(isset($_SESSION['userID']))) {
            header('Location: login.php'); // Redirect To login Page
            exit();
        }
    include_once 'init.php';
    include_once 'layout/head.php';
    include_once 'layout/header.php';

    include_once 'layout/footer.php';
    ?>
    
<?php 
	ob_end_flush();
?>