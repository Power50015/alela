<?php
	ob_start();
    session_start();
        if (!(isset($_SESSION['userID']))) {
            header('Location: login.php'); // Redirect To login Page
            exit();
        }
    include_once 'init.php';
    include_once 'layout/head.php';
    include_once 'layout/header.php';

    if(isset($_GET['do'])){
        if($_GET['do'] == "del"){
            if(isset($_GET['factory'])){

				// Select All Data Depend On This ID

				$check = checkItem('idFactory', 'factory', $_GET['factory']);

				// If There's Such ID Show The Form

				if ($check > 0) {

					$stmt = $con->prepare("DELETE FROM factory WHERE idFactory = :zid");
                    
					$stmt->bindParam(":zid", $_GET['factory']);

					$stmt->execute();

                    $Msg = "تم الحذف";

				} else {

                    $Msg = "لم يتم الحذف";
				}

            }else{
                header('Location: factory.php'); // Redirect To login Page
                exit();
            }
        }else{
            header('Location: factory.php'); // Redirect To login Page
            exit();
        }
    }
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>بيانات المصانع</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($Msg)){?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?=($Msg)?></strong>
            </div>
            <?php }?>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>رقم المصنع</th>
                                <th>أسم المصنع</th>
                                <th>عرض</th>
                                <th>تعديل</th>
                                <th>حذف</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $allItems = getAllFrom("*", "factory");
                                    foreach ($allItems as $value) {
                                ?>
                            <tr>
                                <td><?=($value["idFactory"])?></td>
                                <td><?=($value["factoryName"])?></td>
                                <td><a href="showfactory.php?factory=<?=($value["idFactory"])?>" class="btn btn-primary btn-xs">عــــــــرض</a></td>
                                <td><a href="editfactory.php?factory=<?=($value["idFactory"])?>" class="btn btn-warning btn-xs">تــــــعديل</a></td>
                                <td>
                                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                            data-target=".bs-example-modal-lg-<?=($value["idFactory"])?>">حـــــــذف
                                    </button>

                                    <div class="modal fade bs-example-modal-lg-<?=($value["idFactory"])?>" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span
                                                            aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">حذف المصنع</h4><h4><?=($value["factoryName"])?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                    <h4>تحذير هام</h4>
                                                    <p>في حاله حذف  المصنع من قواعد البيانات فا سيتم حذف جميع بياناته و ملفاته من قواعد البيانات يشمل في ذلك فواتيره و كل معملاته .....إلخ</p>
                                                    <p>ارجو التأكد قبل الحذف و المبرمج غير مسول عن ارجاع البيانات في حاله الحذف مع عدم اخذ النسخ الاحتياطيه</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="factory.php?do=del&factory=<?=($value["idFactory"])?>" type="button" class="btn btn-danger" >حذف على أى حال</a>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">إلغاء عمليه الحذف و الإحتفاظ بالبيانات</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div></td>
                            </tr>
                                    <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php 
    include_once 'layout/footer.php';
	ob_end_flush();
?>