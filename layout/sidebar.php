<div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <div class="site_title"><i class="fa fa-paw"></i> <span>Alela!</span></div>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="build/images/img.jpg" alt="Avtar" class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span><?=(user_access($_SESSION['userAccess']))?></span>
                        <h2><?=($_SESSION['userName'])?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>الأكتر أستخداماً</h3>
                        <ul class="nav side-menu">
                            <li><a href="index.php"><i class="fa fa-home"></i>الرئيسيه</a></li>
                            <li><a><i class="fa fa-edit"></i>فاتوره صادره<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">فاتوره جديدة</a></li>
                                    <li><a href="#">عرض الفواتير</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-paper-plane-o"></i>شاحنه صادره<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">شحنه جديدة</a></li>
                                    <li><a href="#">عرض شحنات</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-car"></i>نقله<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">نقله جديدة</a></li>
                                    <li><a href="#">عرض النقلات</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-calculator"></i>فاتوره وارده<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="addsupplierbill.php">فاتوره جديدة</a></li>
                                    <li><a href="#">عرض الفواتير</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-caret-square-o-left"></i>شاحنه وارده<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">شحنه جديدة</a></li>
                                    <li><a href="#">عرض شحنات</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-usd"></i>مستحقات العملاء<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">دفعه جديده</a></li>
                                    <li><a href="#">عرض المستحقات</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-line-chart"></i>ملخصات<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">ملخص يومى</a></li>
                                    <li><a href="#">ملخص أسبوعى</a></li>
                                    <li><a href="#">ملخص شهرى</a></li>
                                    <li><a href="#">ملخص سنوى</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>الأقل أستخداماً</h3>
                        <ul class="nav side-menu">
                            <li><a href="#"><i class="fa fa-clone"></i>النسخ الإحتياطيه</a></li>
                            <li><a><i class="fa fa-user-circle-o"></i>العملاء<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="customers.php">عرض العملاء</a></li>
                                    <li><a href="addcustomers.php">إضافه عميل</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-linode"></i>المنتجات<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="products.php">عرض المنتجات</a></li>
                                    <li><a href="addproducts.php">إضافه منتج</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-user-circle"></i>الموردين<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="suppliers.php">عرض الموردين</a></li>
                                    <li><a href="addsuppliers.php">إضافه مورد</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-user-o"></i>المستخدمين<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="users.php">عرض المستخدمين</a></li>
                                    <li><a href="adduser.php">إضافه مستخدم</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-taxi"></i>السائقين<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="driver.php">عرض السائقين</a></li>
                                    <li><a href="adddriver.php">إضافه سائق</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-building"></i>المخازن<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="store.php">عرض المخازن</a></li>
                                    <li><a href="addstore.php">إضافه مخزن</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-university"></i>المصانع<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="factory.php">عرض المصانع</a></li>
                                    <li><a href="addfactory.php">إضافه مصنع</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-money"></i>مستحقات للموردين<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">دفعه جديده</a></li>
                                    <li><a href="#">عرض المستحقات</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="الإعدادت">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="صفحه كامله" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="logout.php">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>