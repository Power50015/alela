<?php
	ob_start();
    session_start();
        if (!(isset($_SESSION['userID']))) {
            header('Location: login.php'); // Redirect To login Page
            exit();
        }
    include_once 'init.php';
    include_once 'layout/head.php';
    include_once 'layout/header.php';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Get Variables From The Form

        $supplierName 		= $_POST['name'];
        $_POST['user'] == ""?$supplierUser=Null:$supplierUser= $_POST['user'];
        $_POST['pass'] == ""?$supplierPass=Null:$supplierPass= $_POST['pass'];


        // Check If Category Exist in Database

        $check = checkItem("supplierName", "supplier", $supplierName);

        if ($check == 1) {

            $theMsg = 'اسم المستخدم موجود بالفعل في قواعد البيانات';
            $stat = false;

        } else {
                do {
                    $userId=randomID();
                    $checkid = checkItem("idsupplier", "supplier", $userId);
                } while ($checkid == 1);
            

            // Insert Category Info In Database

            $stmt = $con->prepare("INSERT INTO 

            supplier(`idsupplier`, `supplierName`, `supplierUser`, `supplierPassword`)

            VALUES(:zcheckid, :zsupplierName, :zsupplierUser, :zsupplierPass)");

            $stmt->execute(array(
                'zcheckid'  	=> $userId,
                'zsupplierName' 	=> $supplierName,
                'zsupplierUser' 	=> $supplierUser,
                'zsupplierPass' 	=> $supplierPass,
            ));

            // Echo Success Message

            $theMsg = ' تم إضافه المورد ' . $supplierName ." برقم " .  $userId;
            $stat = true;

        }

    }
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>إضافه عميل جديد</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (isset($theMsg) && $stat == true){?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?=($theMsg)?></strong>
            </div>
            <?php }?>
            <?php if (isset($theMsg) && $stat == false){?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong> <?=($theMsg)?></strong>
            </div>
            <?php }?>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <br/>
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">اسم المورد
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required"
                                           class="form-control col-md-7 col-xs-12" name="name" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">اسم المستخدم
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="user"
                                           class="form-control col-md-7 col-xs-12"  autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">كلمه السر</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="middle-name" class="form-control col-md-7 col-xs-12"  autocomplete="off" type="text" name="pass"
                                           name="middle-name">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary col-sm-12">اضف</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    include_once 'layout/footer.php';
	ob_end_flush();
?>