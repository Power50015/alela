<?php

// Error Reporting

ini_set('display_errors', 'On');
error_reporting(E_ALL);

//DB conntion
$dsn = 'mysql:host=localhost;dbname=alela';
$user = 'root';
$pass = '';
$option = array(
	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);

try {
	$con = new PDO($dsn, $user, $pass, $option);
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	echo 'Failed To Connect' . $e->getMessage();
}

$sessionUser = '';

if (isset($_SESSION['user'])) {
	$sessionUser = $_SESSION['user'];
}


// Include The Important Files

include 'function.php';
